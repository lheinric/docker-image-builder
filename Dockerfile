FROM docker:18

# Add the previously built app binary to the image
COPY main  /

ENTRYPOINT ["/main"]
